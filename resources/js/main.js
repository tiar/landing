import { createApp } from 'vue'
import { Quasar, Notify, LoadingBar } from 'quasar'
import axios from 'axios'
import root from './layouts/root'
import routes from './routes'
import portfolio from './layouts/portfolio'

const csrf = document.head.querySelector('meta[name="csrf-token"]')
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
if (csrf !== null) axios.defaults.headers.common['X-CSRF-TOKEN'] = csrf.textContent
axios.interceptors.response.use(
    (response) => {
        return response
    },
    (error) => {
        if (error.response.status === 401) {
            Notify.create({
                message: 'please login',
                type: 'warning',
            })
            console.log(401)
            console.log(error.response)
        }
        return Promise.reject(error)
    },
)

const app = createApp(process.env.MIX_MAIN_WEBSITE_DEVELOP === 'true' ? root : portfolio)
app.use(Quasar, {
    plugins: { Notify, LoadingBar },
    // config: { dark: store.getters['user/getTheme'] },
})
app.use(routes)
app.mount('#app')
