import { createRouter, createWebHistory } from 'vue-router'

export default createRouter({
    history: createWebHistory('/'),
    routes: [
        {
            path: '/',
            component: () => import('./pages/home.vue'),
            name: 'home',
        },
        {
            path: '/contact',
            component: () => import('./pages/contact.vue'),
            name: 'contact',
        },
        {
            path: '/vacations',
            component: () => import('./pages/vacations.vue'),
            name: 'vacations',
        },
        {
            path: '/:catchAll(.*)',
            name: 'NotFound',
            component: () => import('./pages/notFound.vue'),
        },
    ],
})
